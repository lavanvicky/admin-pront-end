/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
import api from '../utils/backend-api'
import createPersistedState from 'vuex-persistedstate'

import Modules from './modules/Modules'
import Packages from './modules/Packages'
import AdminProfile from './modules/AdminProfile'
import Teachers from './modules/Teachers'
import Students from './modules/Student'
import studentdetails from './modules/Students'
import Notifications from './modules/Notifications'
import Clubs from './modules/Clubs'
import Products from './modules/Products'
import BlockList from './modules/BlockList'
import {
  Admin,
  Club
} from "../components/models";
import gallary from './modules/gallary'
Vue.use(Vuex)

export const store = new Vuex.Store({
  plugins:[createPersistedState()],
  modules: {
    Modules,
    Packages,
    AdminProfile,
    Teachers,
    Students,
    Notifications,
    Clubs,
    Products,
    BlockList,
    gallary,
    studentdetails,


  },
  state: {
    loading: '',
    pagination: '',
    token: '',
    admin: new Admin(),
    club: new Club(),
    schoolName: '',
    phone: '',
    startDate: '',
    description: '',
    benefits: '',
    opentime: '',
    closetime: '',
    email: '',
    adminname: '',
    adminphone: '',
    admingender: '',
    adminaddress: '',

    adminemail: '',
    adminpassword: '',
    policy: '',
    Fullname: '',
    claddress: '',
    CardNumber: '',
    ExpireDate: '',
    CVV: '',
    billingAddress: '',

    earningData: '',
    // modules: '',
    packages: '',
    studentData:'',
    teacherData:'',
    adminPackages:'',
    earningStatementData: '',
    totalEarning: '',
    earningChart: '',
      studentChart: '',
      teacherChart: '',
      studentClass: '',
      studentUpcomingClass: '',
      studentPastClass: '',
    // modules: '',
    packages: '',
    msg:'',
    success:false
  },
  mutations: {
    setLoading (state, {loading}) {
      state.loading = loading
    },
    setToken(state, token) {
      state.token = token
    },
    setUser(state, user) {
      state.user = user
    },
    setEarningsData (state, earnings) {
      state.earningData = earnings
    },
    setEarningsStatementData (state, earnings) {
      state.earningStatementData = earnings
    },
    setTotalEarning (state, earning) {
      state.totalEarning = earning
    },
      setEarningChart (state, earning) {
      state.earningChart = earning
    },
      setStudentChart (state, student) {
      state.studentChart = student
    },
      setTeacherChart (state, teacher) {
      state.teacherChart = teacher
    },
      setStudentDetails (state, student) {
      state.studentDetails = student
    },
      setStudentClass (state, student) {
      state.studentClassDetails = student
    },
      setTeacherEvents (state, teacher) {
      state.teacherEvents = teacher
    },
      setStoreTeacherEvents (state, teacher) {
      state.storeTeacherEvents = teacher
    },
    //   setStudentUpcomingClass (state, student) {
    //   state.studentUpcomingClass = student
    // },
    //   setStudentPastClass (state, student) {
    //   state.studentPastClass = student
    // },
    // setModulesData (state, modules) {
    //   state.modules = modules
    // },
    setPackagesData (state, packages) {
      state.packages = packages
    },
    setStudentData (state, studentData) {
      state.studentData = studentData
    },
    setTeacherData (state, teacherData) {
      state.teacherData = teacherData
    },
    setClubAdminNull(state) {
      state.Clubs = new Clubs()
      state.AdminProfile = new AdminProfile()
    },
    setAdminPackages(state, adminPackages) {
      state.adminPackages = adminPackages;
    },
    setMsg(state,{success,msg}) {
      state.success = success
      state.msg = msg
    }

  },
  getters: {

  },
  actions: {
    resetAccount({
      commit
    }, email) {
      commit("setLoading", {
        loading: true
      });
      var formData = new FormData();
      formData.append('email', email);
      return new Promise(function (resolve, reject) {
        api.postData("auth/recovery", formData)
          .then(res => {
            console.log(res);

            if (res.status == 200) {
              console.log("Successfully reset 200")
              commit("setLoading", {
                loading: false
              });
              resolve(res.data);
            } else {
              console.log("error 404")
              commit("setLoading", {
                loading: false
              });
              reject(res.data);
            }
          })
          .catch(function (error) {
            console.log(error);
            commit("setLoading", {
              loading: false
            });
            reject(error);
          }).then(function () {
            reject("Done")
          })
      });
    },
    resetPassword({
      commit
    }, data) {
      console.log("resetPassword")
      console.log(data)
      return new Promise(function (resolve, reject) {
        api.postData("auth/reset", data)
          .then(res => {
            console.log(res);

            if (res.status == 200) {
              console.log("Successfully reset 200")
              commit("setLoading", {
                loading: false
              });
              resolve(res.data);
            } else {
              console.log("error 404")
              commit("setLoading", {
                loading: false
              });
              reject(res.data);
            }
          })
          .catch(function (error) {
            console.log(error);
            commit("setLoading", {
              loading: false
            });
            reject(error);
          }).then(function () {
            reject("Done")
          })
      });
    },
    updateUser({
      commit
    }, {
      user,
      token
    }) {
      commit("setToken", token)
      commit("setUser", user)
    },
    logout({
      commit
    }) {
      commit("setNotice", {})
      commit("setSnackbar", {})
      commit("setUser", {})
      commit("setadmin", {})
      commit("setclub", {})
      commit("setToken", null)
      commit("setOrderList", {})
      commit("setCustomers", {})
      commit("setPagination", {})
      commit("setLoading", {})
      commit("setNotice", {})
      commit("setSnackbar", {})
      commit("setMode", {})
      commit("setCustomer", {})
      commit("setCustomersData", {})
      commit("setEarning", {})
      commit("setEarningsData", {})
      commit("setEarningsStatementData", {})
      commit("setWithdrawData", {})
      commit("setEarningChart", {})
      commit("setStudentChart", {})
      commit("setTeacherChart", {})
      commit("setStudentClass", {})
      commit("setStudentDetails", {})
      commit("setTeacherEvents", {})
      commit("setStoreTeacherEvents", {})
      // commit("setStudentUpcomingClass", {})
      // commit("setStudentPastClass", {})
      commit("setLogs", {})
      commit("setLogsData", {})
      commit("setAdminPackages", {})
    },
    AddStudent ({
      commit
    }, data) {
      return new Promise(function (resolve, reject) {
        commit('setLoading', {
          loading: true
        })
        console.log('AddStudent Called')
        console.log(data)
        api.postData('admin/teacher/signup', data).then(res => {
            commit('setLoading', {
              loading: false
            })
            resolve(res)
          })
          .catch((error) => {
            commit('setLoading', {
              loading: false
            })
            reject(error)
          })
      })
    },
    AddTeacher({
      commit
    }, data) {
      return new Promise(function (resolve, reject) {
        commit('setLoading', {
          loading: true
        })
        console.log('AddTeacher Called')
        console.log(data)
        api.postData('admin/teacher/signup', data).then(res => {
            commit('setLoading', {
              loading: false
            })
            resolve(res)
          })
          .catch((error) => {
            commit('setLoading', {loading: false})
            reject(error)
          })
      })
    },
    // getAllClasses ({commit}) {
    //   console.log('GetAllClasses Called')
    //   commit('setLoading', { loading: true })
    //   api.getData('admin/module').then(res => {
    //     console.log('Modules Retrived Successfully')
    //     var modules = res.Data
    //     console.log(res)
    //     commit('setModulesData', modules)
    //     commit('setLoading', { loading: false })
    //   })
    // },
    getAllPackages({
      commit
    }) {
      console.log('GetAllPackages Called')
      commit('setLoading', {
        loading: true
      })
      api.getData('admin/package').then(res => {
        console.log('Packages Retrived Successfully')
        console.log(res)
        var packages = res.data
        commit('setPackagesData', packages)
        commit('setLoading', {
          loading: false
        })
      })
    },
    getEarningsTotalAmount({
      commit
    }) {
      commit('setLoading', {
        loading: true
      })
      api.getData('driver/earning').then(res => {
        const data = res.data
        console.log(res)
        commit('setEarningsData', data)
        // let items=[]
        console.log(data[0])
        commit('setLoading', {
          loading: false
        })
      })
    },
    GetStudentCounts() {
      // axios
      // .get('')
      // .then()
      // .catch(error =>{
      //     console.log(error)
      // });
      // api.getData('').then(res=>{
      //     console.log(res.Data);
      // }
      console.log('Hi')
    },
    getStudentData ({commit}) {
      commit("setLoading", { loading: true });
      api.getData("admin/student/").then(res => {
          const data = res.data;
          commit("setStudentData", data)
           console.log(data);
          commitPagination(commit, data);
          commit("setLoading", { loading: false });
      });
  },
  getClassById ({ commit }, id) {
    console.log('this is from store')
    // commit('setLoading', { loading: true })
    if (id) {
      api.getData('admin/module/' + id).then(
            res => {
              const modules = res.data
              commit('setModulesData', { modules })
            },
            err => {
              console.log(err)
            }
        )
    }
  },
     getTeacherData({commit}){
      commit("setLoading", { loading: true });
      api.getData("admin/teacher/").then(res => {
          const data = res.data;
          commit("setTeacherData", data)
           console.log(data);
          commitPagination(commit, data);
          commit("setLoading", { loading: false });
      });
  },
  loadAdminPackages({
    commit
  }) {
    api.postData("auth/driver/products").then(res => {
      commit("setAdminPackages", res.data);
      console.log(res.data)
    });
  },
  addAdmin({
    commit,
    dispatch
  }, admin) {
    console.log(admin);
    // delete admin.address;
    return new Promise((resolve, reject) => {
      if (true) {
        // admin['gender'] = admin['gender'].toLowerCase();
        console.log(admin);
        api.postData("auth/signup", admin)
          .then(res => {
            const adminRes = res.data.user;
            console.log(res.data.user);
            // commit("setadmin", { adminRes });
            commit("setadmin", {
              adminRes
            });

            sendSuccessNotice(commit, "New admin has been added.");
            closeNotice(commit, 1500);
            console.log("SetAdmin Success");
            console.log(this.admin);
            resolve(res);
          })
          .catch(err => {
            console.log(err);
            sendErrorNotice(commit, "Operation failed! Please try again later. ");
            closeNotice(commit, 1500);
          });
      } else {
        api
          .putData("admins/" + admin.id.toString(), admin)
          .then(res => {
            const admin = res.data;
            commit("setadmin", {
              admin
            });
            sendSuccessNotice(commit, "admin has been updated.");
            closeNotice(commit, 1500);
            resolve(res);
          })
          .catch(err => {
            console.log(err);
            sendErrorNotice(commit, "Operation failed! Please try again later. ");
            closeNotice(commit, 1500);
            resolve(res);
          });
      }
    })
  },

  addClub({
    commit,
    dispatch
  }, club) {
    // delete club.address;

    if (!club.id) {
      console.log(club);
      var formData = new FormData();
      formData.append('club_name', club.club_name);
      formData.append('club_email', club.club_email);
      formData.append('club_phone', club.club_phone);
      formData.append('city', club.city);
      formData.append('door', club.door);
      formData.append('floor', club.floor);
      formData.append('streetNumber', club.streetNumber);
      formData.append('zipCode', club.zipCode);
      formData.append('admin_id', club.admin_id);
      formData.append('admin_package_id', club.admin_package_id);
      // formData.append('description', club.description);
      // formData.append('benefit', club.benefit);
      // formData.append('attachment', club.attachment);
      // formData.append('start_date', club.start_date);
      // formData.append('opendays', club.opendays);
      // formData.append('club_start_time', club.club_start_time);
      // formData.append('club_end_time', club.club_end_time);
      // formData.append('club_logo', club.club_logo);
      // formData.append('club_cover', club.club_cover);
      formData.append('cvr_number', club.cvr_number);
      console.log(formData);
      api
        .postData("driver/club/create", formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
        .then(res => {
          const club = res.data;
          console.log(res.data);
          commit("setclub", {
            club
          });
          sendSuccessNotice(commit, "New club has been added.");
          closeNotice(commit, 1500);
          // window.location.replace("/customers");
          // this.$router.push('/customers');
        })
        .catch(err => {
          console.log(err);
          sendErrorNotice(commit, "Operation failed! Please try again later. ");
          closeNotice(commit, 1500);
        });
    } else {
      api
        .putData("club/" + club.id.toString(), club)
        .then(res => {
          const club = res.data;
          commit("setclub", {
            club
          });
          sendSuccessNotice(commit, "Club has been updated.");
          closeNotice(commit, 1500);
        })
        .catch(err => {
          console.log(err);
          sendErrorNotice(commit, "Operation failed! Please try again later.");
          closeNotice(commit, 1500);
        });
    }
  },

  AddPayment ({
    commit
  }, data) {
    return new Promise(function (resolve, reject) {
      commit('setLoading', {
        loading: true
      })
      console.log(data)
      api.postData('admin/pay/new', data).then(res => {

          commit('setLoading', {
            loading: false
          })
          console.log(res.data)
          resolve(res)
        })
        .catch((error) => {
          commit('setLoading', {
            loading: false
          })
          reject(error)
        })
    })
  },
// **************************************************************************************** Justin Ajith
    getAllEarnings({commit}) {
      commit("setLoading", {loading: true});
      api.getData("admin/earning/statement").then(res => {
        const data = res.data;
        commit("setEarningsStatementData", data)
        let items = [];
        commitPagination(commit, data);
        commit("setLoading", {
          loading: false
        });
      });
    },
    getTotalEarning({commit}) {
      commit("setLoading", {loading: true});
      api.getData("admin/earning").then(res => {
        const data = res.data;
        commit("setTotalEarning", data)
        // commitPagination(commit, data);
        commit("setLoading", {
          loading: false
        });
      });
    },
    withdrawalRequest({commit}, request_amount) {
      commit("setLoading", {loading: true});
      api.postData("admin/request/create", {'request_amount':request_amount}).then(res => {
        const data = res.data;
        commit("setLoading", {
          loading: false
        });
      });
    },
      getEarningChart({commit}) {
          commit("setLoading", {loading: true});
          api.getData("admin/earning/monthWise").then(res => {
              const data = res.data;
              commit("setEarningChart", data);
              // commitPagination(commit, data);
              commit("setLoading", {
                  loading: false
              });
          });
      },

      getStudentChart({commit}) {
          commit("setLoading", {loading: true});
          api.postData("admin/monthWise/student").then(res => {
              const data = res.data;
              commit("setStudentChart", data);
              // commitPagination(commit, data);
              commit("setLoading", {
                  loading: false
              });
          });
      },

      getTeacherChart({commit}) {
          commit("setLoading", {loading: true});
          api.getData("admin/cancel/teacher/month").then(res => {
              const data = res.data;
              commit("setTeacherChart", data);
              commit("setLoading", {
                  loading: false
              });
          });
      },

      getSingleStudentDetails({commit}, student) {
          commit("setLoading", {loading: true});
          api.getData("admin/student/"+student).then(res => {
              const data = res.data;
              commit("setStudentDetails", data);
              commit("setLoading", {
                  loading: false
              });
          });
      },

      getStudentClass({commit}, data) {
          commit("setLoading", {loading: true});
          api.postData("admin/student_class", {'student_id':data.studentid, 'package_id':data.packageid}).then(res => {
              const data = res.data;
              commit("setStudentClass", data);
              commit("setLoading", {
                  loading: false
              });
          });
      },

      getTeacherEvents({commit}, teacher) {
          commit("setLoading", {loading: true});
          api.getData("admin/get-teacher-event/"+teacher).then(res => {
              const data = res.data;
              commit("setTeacherEvents", data);
              commit("setLoading", {
                  loading: false
              });
          });
      },

      storeTeacherEvents({commit}, data) {
          commit("setLoading", {loading: true});
          api.postData(`admin/teacher-event/${data.teacher}`, data.datas).then(res => {
              const data = res.data;
              commit("setStoreTeacherEvents", data);
              commit("setLoading", {
                  loading: false
              });
          });
      },
      // getStudentUpcomingClass({commit}, student) {
      //     commit("setLoading", {loading: true});
      //     api.postData("student/viewUpcomingBookings/").then(res => {
      //         const data = res.data;
      //         commit("setStudentUpcomingClass", data);
      //         commit("setLoading", {
      //             loading: false
      //         });
      //     });
      // },
      // getStudentPastClass({commit}, student) {
      //     commit("setLoading", {loading: true});
      //     api.postData("student/ViewPastBookings/").then(res => {
      //         const data = res.data;
      //         commit("setStudentPastClass", data);
      //         commit("setLoading", {
      //             loading: false
      //         });
      //     });
      // },

      updateCancelTime({commit}, cancellation_time_period) {
          commit("setLoading", {loading: true});
          api.postData("admin/club/setting", {'cancellation_time_period':cancellation_time_period}).then(res => {
              const data = res.data;
              commit("setLoading", {
                  loading: false
              });
          });
      },
      confirmMail({commit}, id){
        console.log("confirmMail")
        console.log(id)
        new Promise(function(resolve,reject){
          api.getData("auth/confirmEmail/"+id)
          .then(res => {
            console.log(res);
            resolve(res)
            if(res.status==200){
              let success=false
              let msg="Your email already Confirmed"
              commit('setMsg',{success,msg})
            }else if(res.status==201){
              let success=true
              let msg="Your email has been verified"
              commit('setMsg',{success,msg})
            }else if(res.status==404){
              let success=false
              let msg="Url not found"
              console.log(success,msg)
              commit('setMsg',{success,msg})
            }else{
              let success=false
              let msg="Confirmation Failed"
              console.log(success,msg)
              commit('setMsg',{success,msg})
            }
          })
          .catch(function (error) {
            let success=false
            let msg="Url not found"
            console.log(success,msg)
            commit('setMsg',{success,msg})
          })

        })
      },
      verifyMail({commit}, id){
        console.log("confirmMail")
        console.log(id)
        new Promise(function(resolve,reject){
          api.getData("auth/changeEmail/"+id)
          .then(res => {
            console.log(res);
            resolve(res)
            if(res.status==200){
              let success=false
              let msg="Your email already Confirmed"
              commit('setMsg',{success,msg})
            }else if(res.status==201){
              let success=true
              let msg="Your email has been verified"
              commit('setMsg',{success,msg})
            }else if(res.status==404){
              let success=false
              let msg="Url not found"
              console.log(success,msg)
              commit('setMsg',{success,msg})
            }else{
              let success=false
              let msg="Confirmation Failed"
              console.log(success,msg)
              commit('setMsg',{success,msg})
            }
          })
          .catch(function (error) {
            let success=false
            let msg="Url not found"
            console.log(success,msg)
            commit('setMsg',{success,msg})
          })

        })
      }
  }
})
