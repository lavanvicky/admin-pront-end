/* eslint-disable */
import api from '@/utils/backend-api'

export default {
  namespaced: true,
  state: {
    clubs: ''
  },
  mutations: {
    setClubsData (state, {clubs}) {
      state.clubs = clubs
    }
  },
  actions: {
    getAdminClub ({commit}) {
      console.log('admin club Called')
      return new Promise(function (resolve, reject) {
        api.getData('admin/club')
        .then((response) => {
          console.log('Modules Retrived Successfully')
          var clubs = response.data
          commit('setClubsData', {clubs})
          console.log('Data commited')
          resolve(clubs)
        }).catch((error) => {
          console.log(error)
          reject(error)
        })
      })
    },
  }
}