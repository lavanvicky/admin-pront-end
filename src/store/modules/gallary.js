/* eslint-disable */
import api from '@/utils/backend-api'

export default {
  namespaced: true,
  state: {
    gallary:'',
    pics:[]
  },
  mutations: {
    setGallaryData (state, {gallary}) {
      state.gallary = gallary
    },
    setPicsData (state, {pics}) {
      state.pics = pics
    }
  },
  actions: {
    AddGallaryData({commit},Data){
        return new Promise(function(resolve,reject){
          console.log(Data)
          api.postData('admin/gallery',Data)
          .then((response) => {
            var gallary = response.data
            console.log('Data commited')
            resolve(gallary)
          }).catch((error) => {
            console.log(error)
            reject(error)
          })
  
        })
      },
      getAllGallery ({commit}) {
        return new Promise(function (resolve, reject) {
          api.getData('admin/gallery')
          .then((response) => {
            var pics = response.data
            commit('setPicsData', {pics})
            resolve()
          }).catch((error) => {
            console.log(error)
            reject(error)
          })
        })
      },
  }
}