/* eslint-disable */
import api from '@/utils/backend-api'

export default {
  namespaced: true,
  state: {
    modules: ''
  },
  mutations: {
    setModulesData (state, {modules}) {
      state.modules = modules
    }
  },
  actions: {
    getAllClasses ({commit}) {
      console.log('GetAllClasses Called')
      return new Promise(function (resolve, reject) {
        api.getData('admin/module')
        .then((response) => {
          console.log('Modules Retrived Successfully')
          var modules = response.data
          commit('setModulesData', {modules})
          console.log('Data commited')
          resolve(modules)
        }).catch((error) => {
          console.log(error)
          reject(error)
        })
      })
    },
    getClassDetails(id){
      console.log('getClassDetails Called')
      return new Promise(function (resolve, reject) {
        api.getData('admin/module/'+id)
        .then((response) => {
          console.log('Module Details Retrived Successfully')
          var modules = response.data
          commit('setModulesData', {modules})
          console.log('Data commited')
          resolve(modules)
        }).catch((error) => {
          console.log(error)
          reject(error)
        })
      })
    },
    AddModule ({commit}, ModuleDetails) {
      return new Promise(function (resolve, reject) {
        console.log('AddModule Called')
        console.log(ModuleDetails)
        api.postData('admin/module/create', ModuleDetails).then(res => {
          commit('setModulesData', res.Data)
          resolve(res.Data)
        }).catch((error) => {
          console.log(error)
          reject(error)
        })
        .then(() => {
          console.log('Check your connection and try again')
        })
      })
    },
    classEdit({commit}, Data) {
      // console.log(Data.adminId)
      return new Promise(function (resolve, reject) {
        api.postData('admin/module/' + Data.adminId, Data.moduleDetails).then(res => {
          // console.log(res.Data)
          commit('setModulesData', res.Data)
          resolve(res.Data)
        }).catch((error) => {
          console.log(error)
          reject(error)
        })
        .then(() => {
          console.log('Check your connection and try again')
        })
      })
    },
    getClassById ({ commit }, id) {
      console.log('this is from store')
      // commit('setLoading', { loading: true })
      if (id) {
        api.getData('admin/module/' + id).then(
              res => {
                const modules = res.data
                commit('setModulesData', { modules })
              },
              err => {
                console.log(err)
              }
          )
      }
    },
    deleteClassById ({ commit }, id) {
      console.log('this is from store')
      // commit('setLoading', { loading: true })
      if (id) {
        api.postData('admin/module/delete/' + id).then(
              res => {
                const modules = res.data
                commit('setModulesData', { modules })
              },
              err => {
                console.log(err)
              }
          )
      }
    },

  }
}
