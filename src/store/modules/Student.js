/* eslint-disable */
import api from '@/utils/backend-api'
export default {
  namespaced: true,
  state: {
    Students: '',
    teacherTime:'',
    studentdetails:'',
    studentTeacherdetails:''
  },
  mutations: {
    setModulesData (state, {Students}) {
      state.Students = Students
    },
    setTeacherTime (state, {teacherTime}) {
      console.log(teacherTime);
      state.teacherTime= teacherTime
    },
    setStudentsData (state, {Students}) {
      state.Students = Students
    },
    setStudentData (state, studentdetails) {
      state.studentdetails = studentdetails
    },
    setStudentTeacherData (state, studentTeacherdetails) {
      state.studentTeacherdetails = studentTeacherdetails
    }
  },
  actions: {
    getAllStudents ({commit}) {
      console.log('getAllStudents Called')
      return new Promise(function (resolve, reject) {
        api.getData('admin/student')
        .then((response) => {
          console.log('getAllStudents  Successfully')
          var Students = response.data
          commit('setModulesData', {Students})
          console.log('Data commited')
          resolve(Students)
        }).catch((error) => {
          console.log(error)
          reject(error)
        })
      })
    },
    AddStudent({commit},Data){
      return new Promise(function(resolve,reject){
        console.log("Data")
        console.log(Data)
        api.postData('admin/student/signup',Data)
        .then((response) => {
          var Students = response.data
          console.log('add studentData commited')
          resolve(Students)
        }).catch((error) => {
          console.log(error)
          reject(error)
        })

      })
    },
    getTeacherTime({commit},Data){
      console.log("getTeacherTime")
      api.postData('admin/student_class/get_time',Data)
        .then((response) => {
          console.log('getTeacherTime  Successfully')
          var teacherTime = response.data
          console.log(teacherTime);
          commit('setTeacherTime', {teacherTime})
          // console.log('Data commited')

        })
    },
    adminBookStudentClass({commit},{data}){
      return new Promise(function(resolve,reject){
        console.log("adminBookStudentClass")
        console.log(data)
        api.postData("admin/student_class/book", data)
        .then(res => {
          console.log(res);
          resolve(res)
        })
        .catch(function (error) {
            console.log(error)
            reject(error)
        })
      })
    },
    changeStudentStatus({commit},Data){
      api.postData("admin/student/status/"+Data.studentid, {"status":Data.status})
      .then(res => {
        console.log(res);
      })
      .catch(function (error) {

      })
    },
    getStudentById ({commit}, id) {
      console.log('this is from store')
      if (id) {
        api.getData('admin/student/' + id).then(
        res => {
          var students = res.data
          console.log(students)
          commit('setStudentData', students)
        })
      }
    },
    getClassDetails(id){

    },
    studentEdit ({commit}, Data) {
      return new Promise(function (resolve, reject) {
        api.postData('admin/student/' + Data.StudentId, Data.StudentDetails).then(res => {
          commit('setStudentsData', res.Data)
          resolve(res.Data)
        }).catch((error) => {
        console.log(error)
        reject(error)
      })
      .then(() => {
        console.log('Check your connection and try again')
      })
    })
  },
  getStudentById ({commit}, id) {
    console.log('this is from store')
    if (id) {
      api.getData('admin/student/' + id).then(
      res => {
        var students = res.data
        console.log(students)
        commit('setStudentData', students)
      })
    }
  },
  getStudentTeacherById ({commit}, student_id) {
    console.log('this is from storeuuu')
    if (student_id) {
      api.postData('admin/view_teacher_for_student/' , {"student_id":student_id}).then(
      res => {
        var students = res.data
        console.log(students)
        commit('setStudentTeacherData', students)
      })
    }
  },
}
}
