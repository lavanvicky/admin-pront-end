/* eslint-disable */
import api from '@/utils/backend-api'

export default {
  namespaced: true,
  state: {
    studentdetails: ''
  },
  mutations: {
    setStudentData (state, studentdetails) {
      state.studentdetails = studentdetails
    }
},
actions: {

    getStudentById ({commit}, id) {
        if (id) {
          console.log('this is from store working')
          api.getData('admin/student/' + id).then(
          res => {

            var students = res.data
            console.log(students)
            commit('setStudentData', students)
          })
        }
      },
      getStudentTeacherById ({commit}, id) {
        if (id) {
          console.log('this is from store working')
          api.getData('admin/view_teacher_for_student/',id).then(
          res => {

            var students = res.data
            console.log(students)
            commit('setStudentData', students)
          })
        }
      },
}
}
