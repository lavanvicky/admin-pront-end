/* eslint-disable */
import api from '@/utils/backend-api'

export default {
  namespaced: true,
  state: {
    BlockList: '',
    TeacherBlockList:'',
  },
  mutations: {
    setBlockListData (state, {BlockList}) {
      state.BlockList = BlockList
    },
    setTeacherBlockListData (state, {TeacherBlockList}) {
        state.TeacherBlockList = TeacherBlockList
      }
  },
  actions: {
    getBlockList ({commit}) {
        return new Promise(function (resolve, reject) {
          api.getData('admin/blocked/student')
          .then((response) => {
            var BlockList = response.data
            commit('setBlockListData', {BlockList})
            console.log('Data commited')
            resolve(BlockList)
          }).catch((error) => {
            console.log(error)
            reject(error)
          })
        })
      },
      getTeacherBlockList ({commit}) {
        return new Promise(function (resolve, reject) {
          api.getData('admin/blocked/teacher')
          .then((response) => {
            var TeacherBlockList = response.data
            commit('setTeacherBlockListData', {TeacherBlockList})
            console.log('Data commited')
            resolve(TeacherBlockList)
          }).catch((error) => {
            console.log(error)
            reject(error)
          })
        })
      },
  }
}