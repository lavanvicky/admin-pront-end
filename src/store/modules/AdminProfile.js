/* eslint-disable */
import api from '@/utils/backend-api'

export default {
  namespaced: true,
  state: {
    Subscription: ''
  },
  mutations: {
    setModulesData (state, {Subscription}) {
      state.Subscription = Subscription
    }
  },
  actions: {
    getSubscriptionInformation ({commit}) {
      console.log('getSubscriptionInformation Called')
      return new Promise(function (resolve, reject) {
        api.getData('admin/subscription')
        .then((response) => {
          console.log('Subscription Retrived Successfully')
          var Subscription = response.data
          commit('setModulesData', {Subscription})
          console.log('Data commited')
          resolve(Subscription)
        }).catch((error) => {
          console.log(error)
          reject(error)
        })
      })
    },
    getClassDetails(id){

    }
  }
}
