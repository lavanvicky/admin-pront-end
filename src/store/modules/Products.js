/* eslint-disable */
import api from '@/utils/backend-api'

export default {
  namespaced: true,
  state: {
    Products: ''
  },
  mutations: {
    setProductsData (state, {Products}) {
      state.Products = Products
    }
  },
  actions: {
    getAllProducts ({commit}) {
      console.log('admin club Called')
      return new Promise(function (resolve, reject) {
        api.postData('auth/driver/products')
        .then((response) => {
          console.log('products Retrived Successfully')
          var Products = response.data
          commit('setProductsData', {Products})
          console.log('Data commited')
          resolve(Products)
        }).catch((error) => {
          console.log(error)
          reject(error)
        })
      })
    },
  }
}