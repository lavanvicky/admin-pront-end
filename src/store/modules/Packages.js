/* eslint-disable */
import api from '@/utils/backend-api'

export default {
  namespaced: true,
  state: {
    adminpackages: ''
  },
  mutations: {
    setPackageData (state, adminpackages) {
      state.adminpackages = adminpackages
    }
  },
  actions: {
    getAllPackages ({commit}) {
      console.log('getAllPackages Called')
      api.getData('admin/package').then(res => {
        console.log('Packages Retrived Successfully')
        var packages = res.data
        commit('setPackageData', packages)
      })
    },
    createPackage ({commit}, PackageDetails) {
      return new Promise(function(resolve,reject){
        console.log('create package called')
        console.log(PackageDetails)
        api.postData('admin/package/create', PackageDetails).then(res => {
          console.log(res)
          if(res.status==201){
            resolve(res)
          }
        })
      })
    },
    getPackageById ({commit}, id) {
      if (id) {
        api.getData('admin/package/' + id).then(
        res => {
          var packages = res.data
          console.log(packages)
          commit('setPackageData', packages)
        })
      }
    },
    packageEdit ({commit}, Data) {
      return new Promise(function (resolve, reject) {
        api.postData('admin/package/' + Data.packId, Data.PackageDetails).then(res => {
          commit('setPackageData', res.Data)
          resolve(res.Data)
        }).catch((error) => {
        console.log(error)
        reject(error)
      })
      .then(() => {
        console.log('Check your connection and try again')
      })
    })
  }
  }
}
