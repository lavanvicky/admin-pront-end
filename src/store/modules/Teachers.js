/* eslint-disable */
import api from '@/utils/backend-api'

export default {
  namespaced: true,
  state: {
    Teachers: '',
    TeacherStudentData:'',

  },
  mutations: {
    setTeacherData (state, Teachers) {
      state.Teachers = Teachers
    },
    setTeacherStudentData (state, TeacherStudentData) {
      state.TeacherStudentData = TeacherStudentData
    }
  },
  actions: {
    getAllTeachers ({commit}) {
      console.log('getSubscriptionInformation Called')
      return new Promise(function (resolve, reject) {
        api.getData('admin/teacher')
        .then((response) => {
          console.log('Subscription Retrived Successfully')
          var Teachers = response.data
          commit('setTeacherData', {Teachers})
          console.log('Data commited')
          resolve(Teachers)
        }).catch((error) => {
          console.log(error)
          reject(error)
        })
      })
    },
    updateTeacher({commit},adminData){
      console.log(adminData)
      return new Promise(function(resolve,reject){
        api.postData("admin/teacher/"+adminData.teacherId, adminData)
        console.log("UpdateTeacher")
          .then(res => {
            console.log(res.Data)
          })

        })
        .catch(function (error) {

        })

    },
    AddTeacher({commit}, data) {
      return new Promise(function (resolve, reject) {
        commit('setLoading', {loading: true})
        console.log('AddTeacher Called')
        console.log(data)
        api.postData('admin/teacher/signup', data).then(res => {
            commit('setLoading', {
              loading: false
            })
            resolve(res)
          })
          .catch((error) => {
            commit('setLoading', {loading: false})
            reject(error)
          })
      })
    },
    changeTeacherStatus({commit},Data){
        api.postData("admin/teacher/status/"+Data.teacherid, {"status":Data.status})
        .then(res => {
          console.log(res);
        })
        .catch(function (error) {

        })
    },
    setTeachersSchedule({commit},ScheduleData){
        // console.log("setTeachersSchedule")
        console.log(ScheduleData)
        // console.log(ScheduleData[0])
        // console.log(ScheduleData[1])
        // console.log(ScheduleData[2])
        // console.log(ScheduleData[3])

        for(var x=0;x<ScheduleData[1].length;x++){
          var length=ScheduleData[1][x].length
          for(var y=0;y<length;y++){
              // console.log(ScheduleData[1][x][y])
              ScheduleData[1][x][y]=Object.assign({},ScheduleData[1][x][y])
          }
        }
        var passingData={
          day:ScheduleData[0],
          times:ScheduleData[1],
          date_from:ScheduleData[2],
          date_to:ScheduleData[3],
          teacher_id:ScheduleData[4],
        }
        api.postData("admin/calender/teacher/create",passingData)
        .then(res => {
          console.log(res);
        })
        .catch(function (error) {

        })

    },
    getTeacherById ({commit}, id) {
  if (id) {
    api.getData('admin/teacher/' + id).then(
    res => {
      var teachers = res.data
      commit('setTeacherData', teachers)
    })
  }
},
getTeacher_studentById ({commit}, teacher_id) {
  console.log(teacher_id)
  if (teacher_id) {
    api.postData('admin/teacher_students' ,{'teacher_id':teacher_id}).then(
    res => {
      var teachers = res.data
      commit('setTeacherStudentData', teachers)
    })
  }
},
  }
}
