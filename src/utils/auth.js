/* globals Store */
import api from './backend-api'
/* eslint-disable */


export default {
    login(data, cb) {
        cb = arguments[arguments.length - 1]
        api.login('auth/login', data).then((res) => {
            console.log('Well res');

            console.log("Responce", res)
            const token = res.data.token
            const user = res.data.user
            Store.dispatch('updateUser', { user, token })
            Store.state.SidebarStatus=true;
            if (cb) cb(true, null)
            this.onChange(true)
        }, (err) => {
            console.log('Well res');

            console.log("error", err)
            if (cb) cb(false, err)
            this.onChange(false)
        })
    },
    getToken() {
        return Store.state.token
    },
    logout(cb) {
        // delete localStorage.token
        // Store.commit('setToken', null)
        Store.state.SidebarStatus=false;
        Store.dispatch('logout')
        if (cb) cb(false)
        this.onChange(false)
    },
    loggedIn() {
      console.log(Store)
      return Store.state.token
    },
    onChange() {}
}
