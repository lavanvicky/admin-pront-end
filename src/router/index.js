/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'

// components
import Dashboard from '@/components/Dashboard'

import Charts from '@/components/pages/Charts'
// auth
import ConfirmSignup from '@/components/auth/ConfirmSignup'
import Login from '@/components/auth/Login'
import NewPassword from '@/components/auth/NewPassword'
import ResetPassword from '@/components/auth/ResetPassword'
import ResetPasswordVerification from '@/components/auth/ResetPasswordVerification'
import Signup from '@/components/auth/Signup'
import Signup2 from '@/components/auth/Signup2'
import Verification from '@/components/auth/Verification'

// classes
import Classes from '@/components/classes/Classes'
import CreateClass from '@/components/classes/CreateClass'
// import SelectTime from '@/components/classes/SelectTime.vue'
import Viewclass from '@/components/classes/Viewclass'
import EditClass from '@/components/classes/EditClass'

// error
import ErrorPage from '@/components/error/404'

// package
import Packages from '@/components/package/Packages'
import CreatePackage from '@/components/package/CreatePackage'
import ViewPackage from '@/components/package/ViewPackage.vue'
import StudentViewPack from '@/components/package/StudentViewPack.vue'
import EditPackage from '@/components/package/editpackage'


// pages
  // sasee
import GoToBooking from '@/components/pages/sasee/GoToBooking.vue'
import Invoice from '@/components/pages/sasee/Invoice'

  // student
import AddNewStudent from '@/components/pages/student/AddNewStudent'
import EditStudent from '@/components/pages/student/EditStudent'
import Students from '@/components/pages/student/Students'

  // teacher
import AddNewTeacher from '@/components/pages/teacher/AddNewTeacher'
import TeacherSchedule from '@/components/pages/teacher/TeacherSchedule'
import ConfirmTeacherClasses from '@/components/pages/teacher/ConfirmTeacherClasses'
import Teachers from '@/components/pages/teacher/Teachers'


import AddClasses from '@/components/pages/AddClasses'
import BlockList from '@/components/pages/BlockList'
import Settings from '@/components/pages/Settings'
import Home from '@/components/pages/Home'
import TotalEarnings from '@/components/pages/TotalEarnings'

// Adminprofile
import AdminProfile from '@/components/profile/admin/AdminProfile'

// Teacherprofile
import TeacherProfile from '@/components/profile/teacher/TeacherProfile'

// Studentprofile
import StudentProfile from '@/components/profile/student/StudentProfile'

// test
import Notification from '@/components/test/Notification.vue'
import NotificationClick from '@/components/test/NotificationClick'
import NotificationWithoutClick from '@/components/test/NotificationWithoutClick'
import TestSignup from '@/components/test/testSignup'
import Test from '@/components/test/test'
import CalenderSinglePage from '@/components/test/CalenderSinglePage.vue'
import EditProfile from '@/components/profile/admin/EditProfile'
import EditTeacher from '@/components/profile/teacher/EditTeacher'
import ConfirmMail from '@/components/ConfirmMail'


import auth from '@/utils/auth'

function requireAuth(to, from, next) {
  if (!auth.loggedIn()) {
    next({
      path: '/login',
      query: {
        redirect: to.fullPath
      }
    })
  } else {
    next()
  }
}

Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    { path: '/404', component: ErrorPage, name: 'ErrorPage' },
    { path: '*', redirect: '/404' },
    {
      path: '/confirmation/:id',
      component: ConfirmMail,
      name: 'confirmation',
      props: true
    },
    {
      path: '/verification/:id',
      component: ConfirmMail,
      name: 'verification',
      props: true
    },
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard,
      children: [
        {
          path: '',
          component: Home,
          beforeEnter: requireAuth
        },
        {
          path: 'students',
          component: Students,
          beforeEnter: requireAuth
        },
        {
          path: 'TotalEarnings',
          component: TotalEarnings,
          beforeEnter: requireAuth
        },
        {
          path: 'teachers/edit/:id',
          component: EditTeacher,
          beforeEnter: requireAuth,
          props:true
        },
        {
          path: 'Charts',
          component: Charts,
          beforeEnter: requireAuth
        },
        {
          path: 'BlockList',
          component: BlockList,
          beforeEnter: requireAuth
        },
        {
          path: 'Settings',
          component: Settings
        },
        {
          path: 'teachers',
          component: Teachers,
          beforeEnter: requireAuth
        },
        {
          path: '/classes',
          name: 'Classes',
          component: Classes,
          beforeEnter: requireAuth
        },
        {
          path: '/Packages',
          name: 'Packages',
          component: Packages,
          beforeEnter: requireAuth
        },
        {
          path: '/viewclass/:id',
          name: 'Viewclass',
          component: Viewclass,
          beforeEnter: requireAuth,
          props: true
        },
        {
          path: 'adminprofile',
          component: AdminProfile,
          beforeEnter: requireAuth
        },
        {
          path: '/teacherprofile/:id',
          component: TeacherProfile,
          beforeEnter: requireAuth
        },
        {
          path: '/studentprofile/:id',
          component: StudentProfile,
          beforeEnter: requireAuth
        },
        {
          path: '/notification',
          component: Notification,
          beforeEnter: requireAuth
        },
        {
          path: 'addclasses',
          component: AddClasses,
          beforeEnter: requireAuth
        },
        {
          path: 'ConfirmSignup',
          component: ConfirmSignup,
          beforeEnter: requireAuth
        },
        {
          path: 'invoice',
          component: Invoice,
          beforeEnter: requireAuth
        },
        {
          path: 'test',
          component: Test
        },
        {
          path: 'ConfirmTeacherClasses',
          component: ConfirmTeacherClasses,
          beforeEnter: requireAuth
        },
        {
          path: 'viewpackage/:id',
          component: ViewPackage,
          beforeEnter: requireAuth
        },
        {
          path: 'StudentViewPack/:id',
          component: StudentViewPack,
          beforeEnter: requireAuth,
        },
        {
          path: 'createpackage',
          component: CreatePackage,
          beforeEnter: requireAuth
        },
        {
          path: '/editpackage/:id',
          component: EditPackage
        },
        {
          path: 'CreateClass',
          component: CreateClass,
          beforeEnter: requireAuth
        },
        {
          path: '/editclass/:id',
          component: EditClass,
          beforeEnter: requireAuth
        },
        // {
        //   path: 'SelectTime',
        //   component: SelectTime
        // },
        {
          path: 'GoToBooking',
          component: GoToBooking,
          beforeEnter: requireAuth
        },
        {
          path: 'CalenderSinglePage',
          component: CalenderSinglePage,
          beforeEnter: requireAuth
        },
        {
          path: 'AddNewTeacher',
          component: AddNewTeacher,
          beforeEnter: requireAuth
        },
        {
          path: 'TeacherSchedule',
          component: TeacherSchedule
        },
        {
          path: 'AddNewStudent',
          component: AddNewStudent,
          beforeEnter: requireAuth
        },
        {
          path: 'students/edit/:id',
          component: EditStudent,
          beforeEnter: requireAuth
        }

      ]
    },
    // navication side & out of dashboard components
    {
      path: '/testsignup',
      name: 'TestSignup',
      component: TestSignup
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/complete',
      name: 'Signup2',
      component: Signup2
    },
    {
      path: '/resetpassword',
      name: 'ResetPassword',
      component: ResetPassword
    },
    {
      path: '/newpassword',
      name: 'NewPassword',
      component: NewPassword
    },
    {
      path: '/verification',
      name: 'Verification',
      component: Verification
    },
    // {
    //   path: '/resetPasswordVerification',
    //   name: 'ResetPasswordVerification',
    //   component: ResetPasswordVerification
    // },
    {
      path: '/notificationwithoutclick',
      component: NotificationWithoutClick
    },
    {
      path: '/notificationclick',
      component: NotificationClick
    },
    {
      path: '/firstPackages',
      name: 'Packages',
      component: Packages,
      beforeEnter: requireAuth
    },
    {
      path: '/logout',
      beforeEnter(to, from, next) {
        auth.logout()
        next('/login')
      }
    },
  ]
})
