// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import {
  store
} from './store'
import axios from 'axios'

import fullCalendar from 'vue-fullcalendar'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import DaySpanVuetify from 'dayspan-vuetify'

import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'dayspan-vuetify/dist/lib/dayspan-vuetify.min.css'
import '@fortawesome/fontawesome-free/css/all.css' // Ensure you are using css-loader
import 'vue-croppa/dist/vue-croppa.css'
import Croppa from 'vue-croppa'
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker'
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css'

Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker)
Vue.use(VueRouter)

Vue.use(Croppa)

Vue.use(VueChartkick, {
  adapter: Chart
})
Vue.component('full-calendar', fullCalendar)

Vue.use(Vuetify, {
  theme: {
    primary: '#0051a0',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  },
  iconfont: 'fa'
})

Vue.config.productionTip = false

/* eslint-disable no-new */
Vue.use(DaySpanVuetify, {
  methods: {
    getDefaultEventColor: () => '#1976d2'
  }
})

window.Store = store

new Vue({
  el: '#app',
  router,
  store,
  axios,
  components: {
    App
  },
  template: '<App/>'
})
