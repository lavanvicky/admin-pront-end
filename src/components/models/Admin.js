/* eslint-disable */
export class Admin {
    constructor() {
        this.id = null;
        this.city = '';
        this.door = '';
        this.floor = '';
        this.streetNumber = '';
        this.zipCode = '';
        this.name = '';
        this.email = '';
        this.phone = '';
        this.gender = '';
        this.image = "";
        this.password = '';
        this.club_id = null;
        this.address = {
            oneLineAddress: ""
        }
    }
}