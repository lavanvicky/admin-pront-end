/* eslint-disable */

export class Club {
    constructor() {
        this.id = '',
            this.club_name = ''
        this.club_email = ''
        this.club_phone = ''
        this.admin_id = ''
        this.cvr = ''
        this.city = ''
        this.door = ''
        this.floor = ''
        this.streetNumber = ''
        this.zipCode = ''
        this.admin_package_id =''
        this.description = ''
        this.attachment = null;
        this.benefit = ''
        this.start_date = ''
        this.opendays=[]
        this.club_start_time = ''
        this.club_end_time = ''
        this.club_logo = ''
        this.club_cover = ''
        this.club_earnings =''
        this.last_payment_request_amount=''
        this.address = {
            oneLineAddress: ''
        }
    }
}
